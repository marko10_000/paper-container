#! /bin/sh

(
	# Prepare server
	cd "/persistent" &&
	if [ -z "$MEMORY_GB" ]
	then
		echo 'Requires environemt variable MEMORY_GB'
		exit 1
	fi &&
	if [ ! -f 'AddJavaFlags.txt' ]
	then
		echo '' > AddJavaFlags.txt
	fi &&
	ln -svf "$(dirname "$(dirname "$0")")"/paper.jar ./paper.jar &&

	# Load flags
	if expr "$MEMORY_GB" '<' 13 > /dev/null
	then
		FLAGS="$(cat "$(dirname "$0")"/memory12.txt | sed "s/{{MEMORY}}/$MEMORY_GB/g")"
	else
		FLAGS="$(cat "$(dirname "$0")"/memory13.txt | sed "s/{{MEMORY}}/$MEMORY_GB/g")"
	fi &&
	FLAGS="/opt/java/bin/java $FLAGS $(cat AddJavaFlags.txt) -jar paper.jar nogui" &&
	echo "Start java: $FLAGS" &&

	# Start java
	exec $FLAGS
)

# Fall back to shell
echo "Existied with code $?" >&2
exec sh
