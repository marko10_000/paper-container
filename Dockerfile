FROM alpine

RUN apk add --no-cache screen rsync zstd tar lzip gzip grep curl nano mc
COPY . /setup
RUN sh /setup/setup.sh

CMD /startup.sh
VOLUME /persistent
EXPOSE 25565
