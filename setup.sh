#! /bin/sh

cd "$(dirname "$0")" &&
OWN_DIR="$(pwd)" &&

# Download java
sh sources/download.sh sources/adoptium.conf java.tar.gz &&
mkdir -p /opt/java &&
tar -xzf java.tar.gz --strip-components=1 -C /opt/java &&
rm java.tar.gz &&

# Download paper
sh sources/download.sh sources/paper.conf paper.jar &&

# Install start script
cp startup/startup.sh /startup.sh &&
chmod +x /startup.sh &&

# Generate volume directory
mkdir -p /persistent
