#! /bin/sh

# Check args
if [ -z "$1" ]
then
	echo "Config file not given." >&2
	echo "Usage: download.sh [config-file] [output-file]" >&2
	exit 1
fi
if [ -z "$2" ]
then
	echo "Output file not given." >&2
	echo "Usage: download.sh [config-file] [output-file]" >&2
	exit 1
fi

# Load config
for i in $(cat "$1")
do
	echo "$i" | grep -E '^url:' > /dev/null &&
	URL="$(echo "$i" | grep -Po ':.*')" &&
	URL="$(expr substr "$URL" 2 $(expr $(expr length "$URL") - 1))" &&
	continue
	echo "$i" | grep -E '^sha256:' > /dev/null &&
	SHA256="$(echo "$i" | grep -Po ':.*')" &&
	SHA256="$(expr substr "$SHA256" 2 $(expr $(expr length "$SHA256") - 1))" &&
	continue
	echo "Unknown config: $i" >&2
	exit 2
done

# Download and check
TMP_FILE=''
curl -L -o "$2" "$URL" &&
TMP_FILE="$(mktemp)" &&
echo "$SHA256  -" > $TMP_FILE &&
cat "$2" | sha256sum - | diff $TMP_FILE -
TMP_RES="$?"
[ -n "$TMP_FILE" ] && rm -f "$TMP_FILE"
exit $TMP_RES
